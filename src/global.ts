import { Injectable } from "@angular/core";
import { environment } from "src/environments/environment";

export class Globals {

    assets = "assets/img/";
    pdf ="assets/aviso.pdf";
    pdfTerms ="assets/terminostrnsp.pdf";


}