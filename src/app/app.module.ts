import { NgModule } from '@angular/core';
import { BrowserModule, Title, Meta } from '@angular/platform-browser';
import { AppRoutingModule, routableComponents } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component'; 
import { ErrorComponent } from './error/error.component';

import { BrandingComponent } from "./branding/branding.component";
import { ImpresionComponent } from "./impresion/impresion.component";
import { TextilComponent } from './textil/textil.component';
import { CraftingComponent } from "./crafting/crafting.component";
import { EnviosComponent } from "./envios/envios.component";
import { ContactoComponent } from "./contacto/contacto.component";

import { HeaderComponent } from "./header/header.component"; 
import { FooterComponent } from "./footer/footer.component";
import { TycComponent } from "./tyc/tyc.component";
import { AvisoComponent } from "./aviso/aviso.component";

import { Globals } from 'src/global';

import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { InputMaskModule } from 'primeng/inputmask';
import { TabViewModule } from 'primeng/tabview';
import { PanelMenuModule } from 'primeng/panelmenu';
import { InputTextModule } from 'primeng/inputtext';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TextMaskModule } from 'angular2-text-mask';
import { AccordionModule } from 'primeng/accordion';
import { InplaceModule } from 'primeng/inplace';
import { CalendarModule } from 'primeng/calendar';
//import { DropdownModule } from 'primeng/dropdown';
import { InputNumberModule } from 'primeng/inputnumber';
import { CountUpModule } from 'ngx-countup';
import SwiperCore from 'swiper';
import { SwiperModule } from "swiper/angular";
import { NgxPageScrollCoreModule } from 'ngx-page-scroll-core';
import { NgxPageScrollModule } from 'ngx-page-scroll';



@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ErrorComponent,
    HeaderComponent, 
    FooterComponent, 
    TycComponent, 
    AvisoComponent, TextilComponent, ImpresionComponent,
    CraftingComponent, EnviosComponent, BrandingComponent, ContactoComponent
  ],
  imports: [
    BrowserModule, AppRoutingModule,  HttpClientModule, BrowserAnimationsModule, FormsModule, ReactiveFormsModule, InputNumberModule,
    CommonModule, InputMaskModule, TabViewModule, PanelMenuModule, TextMaskModule, AccordionModule, InplaceModule,
    InputTextModule, CalendarModule, CountUpModule, CommonModule, SwiperModule, NgxPageScrollCoreModule,
    NgxPageScrollModule
  ],
  providers: [Globals],
  bootstrap: [AppComponent]
})
export class AppModule { }
