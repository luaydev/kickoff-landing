import { Component, OnInit, ElementRef, ViewChild, AfterViewInit, HostListener,  Input, Output, EventEmitter, OnChanges } from '@angular/core';
import { debounceTime, takeUntil, distinctUntilChanged } from 'rxjs/operators';
import { DOCUMENT, Location } from '@angular/common';
@Component({
  selector: 'app-impresion',
  templateUrl: './impresion.component.html',
  styleUrls: ['./impresion.component.less']
})
export class ImpresionComponent implements OnInit, AfterViewInit, OnChanges {
  public MagentaUnit:any;
  public YellowUnit:any;
  public BlackUnit:any;
  public MagentaUnitBox:any;
  public YellowUnitBox:any;
  public BlackUnitBox:any;
  public ciclesContainer: any;
  public animateStyles:any;
  public innerWidth:any;
  public fliper:any = 0;
  public displayInfo:any = 0;
  public subtitleServ:any;
  public infoServ:any;    
  @HostListener('window:resize', [])
    onResize() {
      this.detectResize();
    }
    @Input() offsetValue = '';
    @Input() windowW = '';
    @Input() HCont = '';
    @Output() sendScrollValue = new EventEmitter<any>();
  constructor(private elem: ElementRef) { }

  ngOnInit(): void {
    this.innerWidth = window.innerWidth;
    document.querySelectorAll('.inCSS').forEach(function(a){
      a.remove()
    })
    this.animateStyle();
   
  }

  detectResize(): void{
    (this.innerWidth - 150) <= window.innerWidth ? this.changeInner() : '';
  }

  ngAfterViewInit(){
    this.ciclesContainer = document.getElementById("circlescontainer")?.offsetWidth;
    this.innerWidth = window.innerWidth;
    console.log('initial width ' + this.innerWidth)
   /* document.querySelectorAll('.inCSS').forEach(function(a){
      a.remove()
    }) */
    if(this.innerWidth < 600) {
      setTimeout(() => {
        this.doFlip(0);
        this.openCard(0);
      }, 500);
    }
  }

  ngOnChanges() { 
    var pathclass =location.pathname.replace('/', '')
    if(pathclass == 'impresion') { 
      this.doFlip(0);
      this.openCard(0);
    }

   /* document.querySelectorAll('.inCSS').forEach(function(a){
      a.remove()
    }) */
   /* var pathclass =location.pathname.replace('/', '')
    console.log(pathclass);
    if(pathclass == 'impresion') {
      setTimeout(() => {
        this.changeInner();
      }, 100);
    }
   // this.changeInner(); */
  }

  changeInner(){
    this.innerWidth = window.innerWidth;
    document.querySelectorAll('.inCSS').forEach(function(a){
      a.remove()
    })
    this.animateStyle();
  }

  doFlip(n:any) {
    if(this.displayInfo >= 1) {
      this.openCard(0)
    } else {}
    this.fliper = n;
    console.log(this.fliper)
  }

  openCard(n:any){
    
    
    this.displayInfo = n;
    console.log(this.displayInfo)
    switch(n){
      case 1:
        this.subtitleServ = " OFFSET";
        this.infoServ = "Producimos todo tipo de materiales impresos  Catálogos comerciales, libros, manuales de imagen, folletos, dípticos, trípticos, papelería de empresa, tarjetas, talonarios, cartas, sobres impresos, facturas, carpetas y en general cualquier otra impresión que incluya sus manipulados.";
        break;
      case 2 :
        this.subtitleServ = " SERVICIO 2";
        this.infoServ = " Láser, prensa digital, sublimación de tinta, tinta sólida, cera térmica, Impresión digital UV.";
        break;
      case 3 :
        this.subtitleServ = " SERVICIO 3";
        this.infoServ = "Cajas de regalo, fundas de celular camisetas, calendarios, carteras, señalización, cojines, llaveros e imanes, portafotos y mucho más.";
        break;
      case 4 :
        this.subtitleServ = " SERVICIO 4";
        this.infoServ = "Front Light, backlight, vinilo de impresión y corte MESH, posters y carteles.";
        break;
      default :
        this.subtitleServ = "";
        this.infoServ = "";
        break;
    }
  }

  animateStyle() {
    this.animateStyles = '';
    this.innerWidth > 600 ?  this.ciclesContainer = document.getElementById("circlescontainer")?.offsetWidth : 
    this.ciclesContainer = document.getElementById("circlescontainer")?.offsetHeight;
    this.innerWidth > 600 ?
    this.MagentaUnit = (this.ciclesContainer - Number(document.getElementById('circleRef')?.offsetWidth))  / 3 : 
    this.MagentaUnit = (this.ciclesContainer - Number(document.getElementById('circleRef')?.offsetWidth))  / 2.4;
    this.YellowUnit = this.MagentaUnit * 2;
    this.BlackUnit = this.MagentaUnit * 3;
    this.MagentaUnitBox = this.MagentaUnit;
    this.YellowUnitBox = this.MagentaUnit * 2;
    this.BlackUnitBox = this.MagentaUnit * 3;
    this.BlackUnit = this.BlackUnit + 'px';
    this.YellowUnit = this.YellowUnit + 'px';
    this.MagentaUnit = this.MagentaUnit + 'px';

    

    this.MagentaUnitBox = this.MagentaUnitBox + 30 + 'px';
    this.YellowUnitBox = this.YellowUnitBox + 25 + 'px';
    this.BlackUnitBox = this.BlackUnitBox + 27 + 'px';
    console.log(this.ciclesContainer)
    console.log( this.MagentaUnit )
    console.log(this.YellowUnit);
    console.log(this.BlackUnit);
    console.log( this.MagentaUnitBox )
    console.log(this.YellowUnitBox);
    console.log(this.BlackUnitBox);
    if(this.innerWidth > 600) {
    
      this.animateStyles = '<style class="inCSS">' +
      ' @-webkit-keyframes move-second { 0% {opacity:0;} 100% {opacity:1;} ' +
      ' 0% { -webkit-transform: translateX(0); }' + 
      ' 100% { -webkit-transform: translateX(' + this.MagentaUnit + '); } }' +
      ' @-moz-keyframes move-second { 0% {opacity:0;} 100% {opacity:1;}' + 
      ' 0% { -moz-transform: translateX(0); } ' + 
      ' 100% { -moz-transform: translateX('+ this.MagentaUnit +'); } }' +
      ' @-o-keyframes move-second { 0% {opacity:0;} 100% {opacity:1;} ' +
      ' 0% { -o-transform: translateX(0); } ' +
      ' 100% { -o-transform: translateX('+ this.MagentaUnit +'); } }' +
      ' @keyframes move-second { 0% {opacity:0;} 100% {opacity:1;}' +
      ' 0% { transform: translateX(0); } ' +
      ' 100% { transform: translateX('+ this.MagentaUnit +'); } }' +

      ' @-webkit-keyframes move-third { 0% {opacity:0;} 100% {opacity:1;}' +
      ' 0% { -webkit-transform: translateX(0); } ' +
      ' 100% { -webkit-transform: translateX('+ this.YellowUnit +'); } } ' +
      ' @-moz-keyframes move-third { 0% {opacity:0;} 100% {opacity:1;}' +
      ' 0% { -moz-transform: translateX(0); }' +
      ' 100% { -moz-transform: translateX('+ this.YellowUnit +'); } }' +
      ' @-o-keyframes move-third { 0% {opacity:0;} 100% {opacity:1;} ' +
      ' 0% { -o-transform: translateX(0); } ' + 
      ' 100% { -o-transform: translateX('+ this.YellowUnit +'); } }' + 
      ' @keyframes move-third { 0% {opacity:0;} 100% {opacity:1;} ' + 
      ' 100% { transform: translateX('+ this.YellowUnit +'); } }' +

      ' @-webkit-keyframes move-four { 0% {opacity:0;} 100% {opacity:1;}' +
      ' 0% { -webkit-transform: translateX(0); } ' +
      ' 100% { -webkit-transform: translateX('+ this.BlackUnit +'); } }' +
      ' @-moz-keyframes move-four { 0% {opacity:0;} 100% {opacity:1;}' +
      ' 0% { -moz-transform: translateX(0); }' +
      ' 100% { -moz-transform: translateX('+ this.BlackUnit +'); } }' + 
      ' @-o-keyframesmove-four { 0% {opacity:0;} 100% {opacity:1;} ' +
      ' 0% { -o-transform: translateX(0); } ' + 
      ' 100% { -o-transform: translateX('+ this.BlackUnit +'); } }' + 
      ' @keyframes move-four { 0% {opacity:0;} 100% {opacity:1;} ' + 
      ' 100% { transform: translateX('+ this.BlackUnit +'); } }'
      +'</style>';
    } else {

      this.animateStyles = '<style class="inCSS">' +
      ' @-webkit-keyframes move-second { 0% {opacity:0;} 100% {opacity:1;} ' +
      ' 0% { -webkit-transform: translateY(0); }' + 
      ' 100% { -webkit-transform: translateY(' + this.MagentaUnit + '); } }' +
      ' @-moz-keyframes move-second { 0% {opacity:0;} 100% {opacity:1;}' + 
      ' 0% { -moz-transform: translateY(0); } ' + 
      ' 100% { -moz-transform: translateY('+ this.MagentaUnit +'); } }' +
      ' @-o-keyframes move-second { 0% {opacity:0;} 100% {opacity:1;} ' +
      ' 0% { -o-transform: translateY(0); } ' +
      ' 100% { -o-transform: translateY('+ this.MagentaUnit +'); } }' +
      ' @keyframes move-second { 0% {opacity:0;} 100% {opacity:1;}' +
      ' 0% { transform: translateY(0); } ' +
      ' 100% { transform: translateY('+ this.MagentaUnit +'); } }' +

      ' @-webkit-keyframes move-third { 0% {opacity:0;} 100% {opacity:1;}' +
      ' 0% { -webkit-transform: translateY(0); } ' +
      ' 100% { -webkit-transform: translateY('+ this.YellowUnit +'); } } ' +
      ' @-moz-keyframes move-third { 0% {opacity:0;} 100% {opacity:1;}' +
      ' 0% { -moz-transform: translateY(0); }' +
      ' 100% { -moz-transform: translateY('+ this.YellowUnit +'); } }' +
      ' @-o-keyframes move-third { 0% {opacity:0;} 100% {opacity:1;} ' +
      ' 0% { -o-transform: translateY(0); } ' + 
      ' 100% { -o-transform: translateY('+ this.YellowUnit +'); } }' + 
      ' @keyframes move-third { 0% {opacity:0;} 100% {opacity:1;} ' + 
      ' 100% { transform: translateY('+ this.YellowUnit +'); } }' +

      ' @-webkit-keyframes move-four { 0% {opacity:0;} 100% {opacity:1;}' +
      ' 0% { -webkit-transform: translateY(0); } ' +
      ' 100% { -webkit-transform: translateY('+ this.BlackUnit +'); } }' +
      ' @-moz-keyframes move-four { 0% {opacity:0;} 100% {opacity:1;}' +
      ' 0% { -moz-transform: translateY(0); }' +
      ' 100% { -moz-transform: translateY('+ this.BlackUnit +'); } }' + 
      ' @-o-keyframesmove-four { 0% {opacity:0;} 100% {opacity:1;} ' +
      ' 0% { -o-transform: translateY(0); } ' + 
      ' 100% { -o-transform: translateY('+ this.BlackUnit +'); } }' + 
      ' @keyframes move-four { 0% {opacity:0;} 100% {opacity:1;} ' + 
      ' 100% { transform: translateY('+ this.BlackUnit +'); } }' +

      ' @-webkit-keyframes move-second-txt { 0% {opacity:0;} 100% {opacity:1;} ' +
      ' 0% { -webkit-transform: translateY(0); }' + 
      ' 100% { -webkit-transform: translateY(' + this.MagentaUnitBox + '); } }' +
      ' @-moz-keyframes move-second-txt { 0% {opacity:0;} 100% {opacity:1;}' + 
      ' 0% { -moz-transform: translateY(0); } ' + 
      ' 100% { -moz-transform: translateY('+ this.MagentaUnitBox +'); } }' +
      ' @-o-keyframes move-second-txt { 0% {opacity:0;} 100% {opacity:1;} ' +
      ' 0% { -o-transform: translateY(0); } ' +
      ' 100% { -o-transform: translateY('+ this.MagentaUnitBox +'); } }' +
      ' @keyframes move-second-txt { 0% {opacity:0;} 100% {opacity:1;}' +
      ' 0% { transform: translateY(0); } ' +
      ' 100% { transform: translateY('+ this.MagentaUnitBox +'); } }' +

      ' @-webkit-keyframes move-third-txt { 0% {opacity:0;} 100% {opacity:1;}' +
      ' 0% { -webkit-transform: translateY(0); } ' +
      ' 100% { -webkit-transform: translateY('+ this.YellowUnitBox +'); } } ' +
      ' @-moz-keyframes move-third-txt { 0% {opacity:0;} 100% {opacity:1;}' +
      ' 0% { -moz-transform: translateY(0); }' +
      ' 100% { -moz-transform: translateY('+ this.YellowUnitBox +'); } }' +
      ' @-o-keyframes move-third-txt { 0% {opacity:0;} 100% {opacity:1;} ' +
      ' 0% { -o-transform: translateY(0); } ' + 
      ' 100% { -o-transform: translateY('+ this.YellowUnitBox +'); } }' + 
      ' @keyframes move-third-txt { 0% {opacity:0;} 100% {opacity:1;} ' + 
      ' 100% { transform: translateY('+ this.YellowUnitBox +'); } }' +

      ' @-webkit-keyframes move-four-txt { 0% {opacity:0;} 100% {opacity:1;}' +
      ' 0% { -webkit-transform: translateY(0); } ' +
      ' 100% { -webkit-transform: translateY('+ this.BlackUnitBox +'); } }' +
      ' @-moz-keyframes move-four-txt { 0% {opacity:0;} 100% {opacity:1;}' +
      ' 0% { -moz-transform: translateY(0); }' +
      ' 100% { -moz-transform: translateY('+ this.BlackUnitBox +'); } }' + 
      ' @-o-keyframesmove-four-txt { 0% {opacity:0;} 100% {opacity:1;} ' +
      ' 0% { -o-transform: translateY(0); } ' + 
      ' 100% { -o-transform: translateY('+ this.BlackUnitBox +'); } }' + 
      ' @keyframes move-four-txt { 0% {opacity:0;} 100% {opacity:1;} ' + 
      ' 100% { transform: translateY('+ this.BlackUnitBox +'); } }'

      +'</style>';
     
    }
    document.head.innerHTML += this.animateStyles;

  }



} 