import { PageScrollService } from 'ngx-page-scroll-core';
import { Component, OnInit, AfterViewInit, ElementRef, ViewChild, Inject, HostListener,  Input, Output, EventEmitter, OnChanges  } from '@angular/core';
import { DOCUMENT, Location } from '@angular/common';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { switchMap } from 'rxjs';
import { Globals } from 'src/global'; 
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.less']
})
export class HeaderComponent implements OnInit, AfterViewInit {
  public ShowSub:boolean = false;
  public ruta:any;
  public windowW:any;
  public menuMob:boolean = false;
  public pdf:any;
  public pdfTerms:any;
  public onlySafari:boolean = false;
  @Input() offsetValue = '';
  @Output() pathValue = new EventEmitter<string>();
  @HostListener('window:resize', [])
  onResize() {
    this.detectResize();
  }
  constructor(private globals: Globals, private location: Location, private route:ActivatedRoute, private pageScrollService: PageScrollService, @Inject(DOCUMENT) private document: any) {
    this.pdf = globals.pdf;
    this.pdfTerms = globals.pdfTerms;
  }
  ngOnInit() {
    this.windowW = window.innerWidth;
  }

  detectResize(){
    this.windowW != window.innerWidth ? this.changeInner() : '';
  }

  changeInner(){
    this.windowW = window.innerWidth
   
  }

  ngOnChanges() {
    this.ruta = this.offsetValue
  }


  ngAfterViewInit() {
    this.ruta = location.pathname.replace('/', '');
    switch( this.ruta) {
      case '': 
        this.goHome();
        break;
      case 'branding': 
        this.goBranding();
        break;
      case 'impresion':
        this.goImpresion();
         break;
      case 'textil':
        this.goTextil();
        break;
      case 'crafting':
        this.goCrafting();
        break;  
      case 'envios':
        this.goEnvios();
         break;
      case 'contacto':
        this.goContacto();
        break;   
    } 
    if (navigator.userAgent.indexOf('Safari') != -1 && 
    navigator.userAgent.indexOf('Chrome') == -1) {
      this.onlySafari = true;
    }
    console.log(this.onlySafari)
  }

  toggleSub(){
   
      console.log(this.ShowSub);
      if(this.ShowSub == false) {
        this.ShowSub = true;
      } else {
        this.ShowSub = false;
      }
    
  }

  toggleMenuMob(){
   // this.toggleSub()
   if(this.windowW < 700) {
    console.log(this.menuMob);
    this.menuMob == false ? this.menuMob = true : this.menuMob = false;
    
   }
   
  }

  public goHome():void{
    
    this.ruta = '';
    this.pathValue.emit('');
    this.location.replaceState('/');
  //  this.toggleMenuMob();
  }

  public goBranding():void{
    /*this.pageScrollService.scroll({
      document: this.document.querySelector('.squares'),
      scrollTarget: '.branding'
    }) */
    //
    this.ruta = 'branding';
    this.pathValue.emit('branding');
    this.location.replaceState('/branding');
  //  this.toggleMenuMob();
  }

  public goImpresion():void{
   
    this.ruta = 'impresion';
    this.pathValue.emit('impresion');
    this.location.replaceState('/impresion');
  //  this.toggleSub();
   // this.toggleMenuMob();
  }

  public goTextil():void{
    this.location.replaceState('/textil');
    this.ruta = 'textil';
    this.pathValue.emit('textil');
  //  this.toggleSub();
  //  this.toggleMenuMob();
  }

  public goCrafting():void{
    this.location.replaceState('/crafting');
    this.ruta = 'crafting';
    this.pathValue.emit('crafting');
   // this.toggleMenuMob();
  }

  public goEnvios():void{
    this.location.replaceState('/envios');
    this.ruta = 'envios';
    this.pathValue.emit('envios');
  //  this.toggleMenuMob();
  }

  public goContacto():void{
    this.ruta = 'contacto';
    this.pathValue.emit('contacto');
    this.location.replaceState('/contacto');
  //  this.toggleMenuMob();
  }
}
