import { Component, OnInit, AfterViewInit, ElementRef, ViewChild, Inject, OnDestroy, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import { Globals } from 'src/global'; 
import { ViewportScroller } from '@angular/common';
import { fromEvent } from 'rxjs';
import { debounceTime } from 'rxjs';
import { untilDestroyed } from 'ngx-take-until-destroy';
import { ScrollService } from '../scroll.service';

@Component({
  selector: 'app-branding',
  templateUrl: './branding.component.html',
  styleUrls: ['./branding.component.less']
})
export class BrandingComponent implements OnInit, AfterViewInit, OnChanges {
  public imgs:any;
  public title:any;
  public subtitle:any;
  public textBranding:any;
  public scllnumber:any;
  public animateBrandStyles:any;
  @Input() offsetValue = '';
  @Input() windowW = '';
  @Input() HCont = '';
  @Output() sendScrollValue = new EventEmitter<any>();
  constructor(private globals: Globals, private ss:ScrollService) {
    this.imgs = globals.assets;
    var pathclass =location.pathname.replace('/', '')
    console.log(pathclass);
   }

  ngOnInit(): void {
    this.title = document.querySelector('.textT');
    this.subtitle = document.querySelector('.textSt');
    this.textBranding = document.querySelector('.txtBranding');
    var pathclass =location.pathname.replace('/', '')
    console.log(pathclass);
  }

  ngAfterViewInit() { 
  
   
   /* if(pathclass== 'branding' ) {
      console.log(this.offsetValue)
      setTimeout(() => {
        this.title.classList.add('mover-brand');
        this.subtitle.classList.add('mover-brand');
        this.textBranding.classList.add('mover-vert');
      }, 1000);
     
    } */
  }

  ngOnChanges() {

  
  
  }


  animateBranding(){
    this.animateBrandStyles = '';
    this.animateBrandStyles  = '<style class="BrandInCSS">' +
    ' @-webkit-keyframes mover-brand { 0% {opacity:0;}' +
    ' 100% {opacity:1;}  0% { -webkit-transform: translateX(50%); } ' + 
    ' 100% { -webkit-transform: translateX(0%); } }' +
    ' @-moz-keyframes mover-brand { 0% {opacity:0;} ' +
    ' 100% {opacity:1;} 0% { -moz-transform: translateX(50%); }' +
    ' 100% { -moz-transform: translateX(0%); } } ' +
    ' @-o-keyframes mover-brand { 0% {opacity:0; } ' +
    ' 100% {opacity:1;} 0% { -o-transform: translateX(50%); } ' +
    '  100% { -o-transform: translateX(0%); } } ' +
    ' @keyframes mover-brand { 0% {opacity:0; } ' + 
    '  100% {opacity:1;} 0% { transform: translateX(50%); } ' + 
    '  100% { transform: translateX(0%); } } ' +
    ' @-webkit-keyframes mover-vert { 0% {opacity:0; } ' + 
    '  100% {opacity:1;} 0% { -webkit-transform: translateY(60%); } ' + 
    '  100% { -webkit-transform: translateY(0%); } } ' + 
    ' @-moz-keyframes mover-vert { 0% {opacity:0; } ' +
    '  100% {opacity:1;} 0% { -moz-transform: translateY(60%); } ' +
    '  100% { -moz-transform: translateY(0%); } } ' +
    ' @-o-keyframes mover-vert { 0% {opacity:0;} ' +
    '  100% {opacity:1;} 0% { -o-transform: translateY(60%); } ' + 
    '  100% { -o-transform: translateY(0%); } } ' + 
    ' @keyframes mover-vert { 0% {opacity:0; } ' +
    '  100% {opacity:1;}0% { transform: translateY(60%); } ' + 
    '  100% { transform: translateY(0%); } }';
     document.head.innerHTML += this.animateBrandStyles;
  }

}
