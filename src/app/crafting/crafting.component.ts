import { Component, OnInit,  Input, Output, EventEmitter, AfterViewInit, OnChanges } from '@angular/core';
import { Globals } from 'src/global';
import { DOCUMENT,  ViewportScroller, Location } from '@angular/common';

@Component({
  selector: 'app-crafting',
  templateUrl: './crafting.component.html',
  styleUrls: ['./crafting.component.less']
})
export class CraftingComponent implements OnInit,  AfterViewInit, OnChanges {
  
  public imgs:any;
  public titleCraft:any;
  public subtitleCraft:any;
  public textCraft:any;
  @Input() offsetValue = '';
  @Input() windowW = '';
  @Input() HCont = '';
  @Output() sendScrollValue = new EventEmitter<any>();
  constructor(private globals: Globals,  private location: Location) {
    this.imgs = globals.assets;
   }

   
  ngOnInit(){
  }

   ngAfterViewInit() { 
  
    this.titleCraft = document.querySelector('.textTC');
    this.subtitleCraft = document.querySelector('.textStC');
    this.textCraft = document.querySelector('.txtCrafting');
  }


   ngOnChanges() {
   /* this.titleCraft = document.querySelector('.textTC');
    this.subtitleCraft = document.querySelector('.textStC');
    this.textCraft = document.querySelector('.txtCrafting');
    
    console.log(this.offsetValue)
    if(this.offsetValue == 'crafting' && location.pathname.replace('/', '') == 'crafting') {
      console.log('entro')
      this.titleCraft.classList.add('mover-craft');
      this.subtitleCraft.classList.add('mover-craft');
      this.textCraft.classList.add('mover-vert');
    } else {
      this.titleCraft.classList.remove('mover-craft');
      this.subtitleCraft.classList.remove('mover-craft');
      this.textCraft.classList.remove('mover-vert');
    }
    
  */
  } 
  

}
