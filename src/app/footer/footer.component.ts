import { Component, OnInit } from '@angular/core';
import { Globals } from 'src/global'; 
@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.less']
})
export class FooterComponent implements OnInit {
  public pdf:any;
  constructor(private globals: Globals) {
    this.pdf = globals.pdf;
   }

  ngOnInit(): void {
  }

}
