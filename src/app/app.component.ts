import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { Globals } from 'src/global';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less'],
  providers: [Globals]
})
export class AppComponent implements OnInit {
  title = 'kickoff-landing';
  
  constructor(){

  }

  ngOnInit(): void {
      
  }
}
