import { NgModule } from "@angular/core";
import { Routes, RouterModule, PreloadAllModules } from "@angular/router";
// inyección de componentes

import { HomeComponent } from "./home/home.component";
import { BrandingComponent } from "./branding/branding.component";
import { ImpresionComponent } from "./impresion/impresion.component";
import { TextilComponent } from "./textil/textil.component";
import { CraftingComponent } from "./crafting/crafting.component";
import { EnviosComponent } from "./envios/envios.component";
import { ContactoComponent } from "./contacto/contacto.component";
import { ErrorComponent } from "./error/error.component";

// rutas
const routes: Routes = [{
    path: '',
    children: [
        { path:'', component: HomeComponent },
        { path:':enlace', component: HomeComponent }
       /* { path: 'home', component: HomeComponent },
        { path: 'branding', component: BrandingComponent },
        { path: 'impresion-servicios', component: ImpresionComponent },
        { path: 'servicios-textiles', component: TextilComponent },
        { path: 'crafting', component: CraftingComponent },
        { path: 'envios', component: EnviosComponent },
        { path: 'contacto', component: ContactoComponent },
        { path: '**', component: ErrorComponent } */
    ]
}];

@NgModule({
    imports: [ RouterModule.forRoot(routes, {preloadingStrategy: PreloadAllModules, scrollPositionRestoration: 'enabled'}) ],
    exports: [ RouterModule ]
})



export class AppRoutingModule {}

export const routableComponents = [
    HomeComponent
]