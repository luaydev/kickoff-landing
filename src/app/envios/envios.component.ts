import { Component, OnInit, AfterViewInit, ElementRef, ViewChild, Inject, HostListener,  Input, Output, EventEmitter, OnChanges  } from '@angular/core';
import { Globals } from 'src/global'; 
@Component({
  selector: 'app-envios',
  templateUrl: './envios.component.html',
  styleUrls: ['./envios.component.less']
})
export class EnviosComponent implements OnInit {
  public imgs:any;
  @Input() offsetValue = '';
  @Input() windowW = '';
  @Input() HCont = '';
  @Output() sendScrollValue = new EventEmitter<any>();
  @Output() viewTyC = new EventEmitter<string>();
   public pdfTerms:any;
  constructor(private globals: Globals) {
    this.imgs = globals.assets;
    this.pdfTerms = globals.pdfTerms;
   }

  ngOnInit(): void {
  }

  viewTerms(n:any) { 
    this.viewTyC.emit(n)
  }

}
