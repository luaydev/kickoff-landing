import { Component, OnInit, Input, Output, OnChanges, EventEmitter } from '@angular/core';
import { Globals } from 'src/global'; 
@Component({
  selector: 'app-contacto',
  templateUrl: './contacto.component.html',
  styleUrls: ['./contacto.component.less']
})
export class ContactoComponent implements OnInit, OnChanges {
  public imgs:any;
  @Input() offsetValue = '';
  @Input() windowW = '';
  @Input() HCont = '';
  @Output() sendScrollValue = new EventEmitter<any>();

  constructor(private globals: Globals,) { 
    this.imgs = globals.assets;
    var pathclass =location.pathname.replace('/', '')
    console.log(pathclass);
  }

  ngOnInit(): void {
  }

  ngOnChanges() {
  }

}
