import { Component, OnInit, AfterViewInit, ElementRef, ViewChild, ChangeDetectorRef,  Inject, OnDestroy, HostListener, OnChanges  } from '@angular/core';
import { DOCUMENT,  ViewportScroller, Location } from '@angular/common';
import { Globals } from 'src/global';
import {  takeUntil, map, debounceTime, distinctUntilChanged  } from 'rxjs/operators';
import { untilDestroyed } from 'ngx-take-until-destroy';
import { ScrollService } from '../scroll.service';
import { SwiperComponent } from "swiper/angular";
import { PageScrollService } from 'ngx-page-scroll-core';
import {FormControl} from '@angular/forms';
import { fromEvent } from 'rxjs';
import { trigger, state, style, animate,transition} from '@angular/animations';
// import Swiper core and required modules
import Swiper, { SwiperOptions, Manipulation, Parallax, Pagination, Navigation  } from "swiper";
Swiper.use([Parallax, Pagination, Navigation]);

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.less'],
  providers: [Globals]
})
export class HomeComponent implements OnDestroy, OnInit, AfterViewInit, OnChanges {
  private swiper: any = false;
  public textBanner :any; 
  public imgs:any;
  public imgBanner:any;
  public scllnumber:any;
  public offset:any;
  public offsetValue:any = '';
  public innerHeight:any;
  public windowH : any;
  public windowW : any = 0;
  public finalwidth:any;
  public HCont :any;
  public infoServ:any;    
  public squaresElem:any;
  public scrollEment :any;
  public viewTyC:any = '';
  @HostListener('window:resize', [])
    onResize() {
      this.detectResize();
    }
  constructor( private location: Location, private ss:ScrollService, private pageScrollService: PageScrollService, @Inject(DOCUMENT) private document: any, private globals:Globals, private cd: ChangeDetectorRef) {
    this.imgs = globals.assets;
   
   }
/*
  ngAfterViewChecked()
  {
    console.log( "!cambios de data !" );
  }  */

  detectResize(){
    this.innerHeight != window.innerHeight ? this.changeInner() : '';
  }
  changeInner(){
    this.windowH = window.innerHeight
    this.windowW = window.innerWidth;
    console.log(this.windowH)
    console.log(this.windowW) 
    console.log( this.windowW * .5)
    this.HCont  = Number( this.windowH) * .70;
    this.finalwidth = 7 *  this.HCont;
  }
  ngOnChanges() { 
     this.changeInner();
  }
  ngOnDestroy(): void {}
  
  addItem(newItem: string) {
    switch(newItem) {
      case '': 
        this.swiper.slideTo(1, 1000, 0);
        break;
      case 'branding': 
        this.swiper.slideTo(2, 1000, 0);
        break;
      case 'impresion':
        this.swiper.slideTo(3, 1000, 0);
        break;
      case 'textil':
        this.swiper.slideTo(4, 1000, 0);
        break;
      case 'crafting':
        this.swiper.slideTo(5, 1000, 0);
        break; 
      case 'envios':
        this.swiper.slideTo(6, 1000, 0);
        break;   
      case 'contacto':
        this.swiper.slideTo(7, 1000, 0);
        break;     
     }
    // this.swiper.slideTo(2, 1000, 0)
     this.offsetValue = newItem;
  // this.scrollEment = document.querySelector('.' + newItem);
  // this.scrollEment.scrollIntoView({ behavior: 'smooth', block: 'start'});
  }
  
  ngOnInit(): void {
    this.changeInner();
   /* fromEvent(window, 'scroll').pipe(untilDestroyed(this), debounceTime(20), distinctUntilChanged()).subscribe(() => {
        this.offset = window.pageXOffset || document.documentElement.scrollLeft || document.body.scrollLeft || 0;
        console.log(this.offset);
      }) */
      this.initSwiper();
  }

  initSwiper() {
    this.swiper =  new Swiper('.swiper', {
       // install FreeMode module if you use core version of Swiper
       parallax: false,
       modules: [ Parallax, Navigation, Pagination],
       loop: true,
       speed: 500,
       slidesPerView: 1,
       allowSlideNext: true,
       allowSlidePrev: true,
       allowTouchMove: false,
       autoHeight: true,
       effect: 'slide',
      /* navigation: {
         nextEl: '.swiper-button-next',
         prevEl: '.swiper-button-prev',
       },   pagination: {
         el: '.swiper-pagination',
         type: 'bullets',
       } */
     });
    
     console.log(location.pathname.replace('/', ''));
     
    
   }

  onScroll(e:any){
    this.offset = (e.target as HTMLElement).scrollTop
    console.log(this.offset)
    if(this.offset > this.windowW * 0.3 && this.offset < this.windowW * 0.7) {
      this.textBanner = document.querySelector('.txt-c');
      if (location.pathname.replace('/', '') != '') {
        this.location.replaceState('');
        this.offsetValue = ''
        setTimeout(() => {
          this.textBanner.classList.add('mover-h');
        }, 100);
        setTimeout(() => {
          this.textBanner.classList.remove('mover-h');
        }, 1500);
      }
    }
    /*
    if(this.offset >= this.windowW * 0.7 && this.offset < this.windowW * 1.4) {
      this.location.replaceState('/branding');
      this.offsetValue = 'branding'
    }
    if(this.offset >= this.windowW * 1.6  && this.offset < this.windowW * 2.3) {
      this.location.replaceState('/impresion');
      this.offsetValue = 'impresion'
    }
    if(this.offset >= this.windowW * 2.8  && this.offset < this.windowW * 3.5) {
      this.location.replaceState('/textil');
      this.offsetValue = 'textil'
    }
    if(this.offset >= this.windowW * 3.8 && this.offset < this.windowW * 4.5) {
      this.location.replaceState('/crafting');
      this.offsetValue = 'crafting'
    }
    if(this.offset >= this.windowW * 4.9  && this.offset < this.windowW * 5.2) {
      this.location.replaceState('/envios');
      this.offsetValue = 'envios'
    }  */
  }

  ngAfterViewInit() {
   
   // this.squaresElem = document.querySelector('.squares');
   // this.innerHeight = window.innerHeight;
    this.textBanner = document.querySelector('.txt-c');
    this.textBanner.classList.add('mover-h');
   /* var pathclass =location.pathname.replace('/', '.')
    if(pathclass == '.') {
      pathclass = '.home';
    } */
   // console.log(pathclass)
   // this.scrollEment = document.querySelector(pathclass);
   // this.scrollEment.scrollIntoView({ behavior: 'smooth', block: 'start'});
    setTimeout(() => {
      this.textBanner.classList.remove('mover-h');
      this.textBanner.classList.add('mover-h');
    }, 1500); 
    this.cd.detectChanges();
  }

  showTyC(e:any){
    console.log(e);
  }

  
}
