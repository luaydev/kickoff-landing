import { Component, OnInit, ElementRef, ViewChild, AfterViewInit, HostListener,  Input, Output, EventEmitter, OnChanges } from '@angular/core';
import { Globals } from 'src/global'; 
@Component({
  selector: 'app-textil',
  templateUrl: './textil.component.html',
  styleUrls: ['./textil.component.less']
})
export class TextilComponent implements OnInit, AfterViewInit, OnChanges {
  public fliper:any;
  public displayInfo:any = 0;
  public subtitleServ:any;
  public infoServ:any;    
  @Input() offsetValue = '';
  @Input() windowW = '';
  @Input() HCont = '';
  @Output() sendScrollValue = new EventEmitter<any>();
  public imgs:any;
  public onlySafari:boolean = false;
  constructor(private globals: Globals) {
    this.imgs = globals.assets;
   }

  ngOnInit(): void {
  }

  ngOnChanges() { 
    var pathclass =location.pathname.replace('/', '')
    if(pathclass != 'textil') { 
      this.displayInfo = 0
    }
  }

  ngAfterViewInit(){
    if (navigator.userAgent.indexOf('Safari') != -1 && 
    navigator.userAgent.indexOf('Chrome') == -1) {
      this.onlySafari = true;
    }
    console.log(this.onlySafari)
  }

  doFlip(n:any) {
    
    this.displayInfo = n;
    console.log(this.displayInfo);
    this.openCard(n)
  }

  openCard(n:any){
    this.infoServ = "";
    switch(n){
      case 1:
        this.infoServ = "";
        break;
      case 2 :
       
        this.infoServ = "Info de servicio 2";
        break;
      case 3 :
       
        this.infoServ = "Info de servicio 3";
        break;
      default :
       
        this.infoServ = "";
        break;
    }
  }

}
