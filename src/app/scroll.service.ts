import { Injectable, Inject, OnDestroy } from '@angular/core';
import { ViewportScroller } from '@angular/common';
import { fromEvent } from 'rxjs';
import { debounceTime, takeUntil, distinctUntilChanged } from 'rxjs/operators';
import { untilDestroyed  } from 'ngx-take-until-destroy'

@Injectable({
  providedIn: 'root'
})
export class ScrollService implements OnDestroy {

  constructor(private vc:ViewportScroller) {

    fromEvent(window, 'scroll').pipe(untilDestroyed(this), debounceTime(50), distinctUntilChanged()).subscribe(() => console.log(vc.getScrollPosition()))

  }
  
  ngOnDestroy(): void {}
}
